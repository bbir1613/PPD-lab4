#include <iostream>
#include "domain/ConcurrentDoublyLinkedList.h"
#include <thread>

void insertIntoList(ConcurrentDoublyLinkedList<int> &concurrentDoublyLinkedList) {
    concurrentDoublyLinkedList.insert(2);
    concurrentDoublyLinkedList.insert(1);
    concurrentDoublyLinkedList.insert(0);
    concurrentDoublyLinkedList.insert(1);
    concurrentDoublyLinkedList.insert(2);
    concurrentDoublyLinkedList.insert(0);
    concurrentDoublyLinkedList.insert(4);
}

void removeFromList(ConcurrentDoublyLinkedList<int> &concurrentDoublyLinkedList) {
    concurrentDoublyLinkedList.remove(0);
    concurrentDoublyLinkedList.remove(1);
    concurrentDoublyLinkedList.remove(2);
    concurrentDoublyLinkedList.remove(3);
}

int main() {
    ConcurrentDoublyLinkedList<int> concurrentDoublyLinkedList;
    std::thread th(insertIntoList, std::ref(concurrentDoublyLinkedList));
    th.join();
    std::thread th1(insertIntoList, std::ref(concurrentDoublyLinkedList));
    std::thread th2(removeFromList, std::ref(concurrentDoublyLinkedList));
    std::thread th3(removeFromList, std::ref(concurrentDoublyLinkedList));
    th1.join();
    th2.join();
    th3.join();
    std::cout << concurrentDoublyLinkedList << std::endl;
//    concurrentDoublyLinkedList.remove(1);
    std::cout << concurrentDoublyLinkedList << std::endl;
    std::cout << concurrentDoublyLinkedList.getSize() << std::endl;
    auto it = concurrentDoublyLinkedList.iterator();
    while (it.get()->hasNext()) {
        std::cout << it.get()->next() << ",";
    }
    std::cout << std::endl;
    auto backit = concurrentDoublyLinkedList.backwardsIterator();
    while (backit.get()->hasNext()) {
        std::cout << backit.get()->next() << ",";
    }
    std::cout << std::endl;
    std::cout << "Hello, World!" << std::endl;
    return 0;
}

