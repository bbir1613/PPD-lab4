//
// Created by Bogdan on 05.11.2016.
//

#ifndef PPD_LAB4_CONCURRENTDOUBLYLINKEDLIST_H
#define PPD_LAB4_CONCURRENTDOUBLYLINKEDLIST_H

#include "../util/util.h"
#include <memory>
#include <mutex>

template<class E>
class ConcurrentDoublyLinkedList {
private:
    struct Node {
        std::shared_ptr<Node> previous;
        std::shared_ptr<Node> next;
        std::shared_ptr<E> element;
        std::mutex mtx;

        Node() {
            DEBUG("Node()")
            previous = std::shared_ptr<Node>(nullptr);
            next = std::shared_ptr<Node>(nullptr);
            element = std::shared_ptr<E>(nullptr);
        }

        Node(std::shared_ptr<Node> previous, std::shared_ptr<Node> next, std::shared_ptr<E> element) {
            DEBUG("Node* previous,Node* next , E* element")
            this->previous = previous;
            this->next = next;
            this->element = element;
        }

        ~Node() {
            DEBUG("~Node()")
        }
    };

    class ForwardIterator {
        std::shared_ptr<Node> current;
    public:
        ForwardIterator(std::shared_ptr<Node> first) {
            current = first;
        }

        bool hasNext() {
            return current.get()->next.get()->element != nullptr;
        }

        E next() {
            current = current.get()->next;
            return *current.get()->element;
        }
    };

    class BackWardsIterator {
        std::shared_ptr<Node> current;
    public:
        BackWardsIterator(std::shared_ptr<Node> last) {
            current = last;
        }

        bool hasNext() {
            return current.get()->previous.get()->element != nullptr;
        }

        E next() {
            current = current.get()->previous;
            return *current.get()->element;
        }
    };

private:
    std::shared_ptr<Node> first;
    std::shared_ptr<Node> last;
    int size;
public:
    ConcurrentDoublyLinkedList() {
        DEBUG("ConcurrentDoublyLinkedList()")
        first = std::shared_ptr<Node>(new Node());
        last = std::shared_ptr<Node>(new Node());
        size = 0;

        first.get()->next = last;
        last.get()->previous = first;
    }

    ~ConcurrentDoublyLinkedList() {
        DEBUG("~ConcurrentDoublyLinkedList()")
        size += 1;
        std::shared_ptr<Node> start = first;
        for (unsigned int i = 0; i < size; ++i) {
            start = start.get()->next;
            start.get()->previous = nullptr;
        }
    }

    int getSize() {
        return size;
    }

    void insert(E e) {
        E *el = new E;
        *el = e;
        std::shared_ptr<E> element = std::shared_ptr<E>(el);
        first.get()->mtx.lock();
        first.get()->next.get()->mtx.lock();
        std::shared_ptr<Node> start = first;
        std::shared_ptr<Node> next = start.get()->next;
        while (next != nullptr && next.get()->element != nullptr &&
               *element.get() > *next.get()->element.get()) {//idk why > ?!
            next.get()->next.get()->mtx.lock();
            start.get()->mtx.unlock();
            start = next;
            next = start.get()->next;
        }
        std::shared_ptr<Node> insertNode = std::shared_ptr<Node>(new Node(start, next, element));
        start.get()->next = insertNode;
        next.get()->previous = insertNode;
        start.get()->mtx.unlock();
        next.get()->mtx.unlock();
        ++size;
    }

    void remove(int position) {
        if (position >= size) {
            return;
        }
        first.get()->mtx.lock();
        first.get()->next.get()->mtx.lock();
        first.get()->next.get()->next.get()->mtx.lock();
        std::shared_ptr<Node> previous = first;
        std::shared_ptr<Node> deletedNode = previous.get()->next;
        std::shared_ptr<Node> next = deletedNode.get()->next;
        for (unsigned int i = 0; i < position; ++i) {
            next.get()->next.get()->mtx.lock();
            previous.get()->mtx.unlock();
            previous = deletedNode;
            deletedNode = previous.get()->next;
            next = deletedNode.get()->next;
        }
        previous.get()->next = next;
        next.get()->previous = previous;
        previous.get()->mtx.unlock();
        next.get()->mtx.unlock();
        deletedNode.get()->mtx.unlock();
        --size;
    }

    E *get(int position) {
        if (position >= size) {
            return nullptr;
        }
        first.get()->mtx.lock();
        first.get()->next.get()->mtx.lock();
        std::shared_ptr<Node> start = first.get()->next;
        first.get()->mtx.unlock();
        for (unsigned int i = 0; i < position; ++i) {
            start.get()->next.get()->mtx.lock();
            start = start.get()->next;
            start.get()->previous.get()->mtx.unlock();
        }
        if (start.get() != nullptr) {
            E *el = start.get()->element.get();
            start.get()->mtx.unlock();
            return el;
        }
        return nullptr;
    }

    std::shared_ptr<ForwardIterator> iterator() {
        return std::shared_ptr<ForwardIterator>(new ForwardIterator(first));
    }

    std::shared_ptr<BackWardsIterator> backwardsIterator() {
        return std::shared_ptr<BackWardsIterator>(new BackWardsIterator(last));
    }

    template<class F>
    friend std::ostream &operator<<(std::ostream &out, const ConcurrentDoublyLinkedList<F> &other);

    template<class F>
    friend std::ostream &operator<<(std::ostream &out, const ConcurrentDoublyLinkedList<F> &other) {
        std::shared_ptr<Node> start = other.first.get()->next;
        if (other.size != 0) {
            for (unsigned int i = 0; i < other.size - 1; ++i) {
                out << *start.get()->element.get() << ",";
                start = start.get()->next;
            }
            out << *start.get()->element.get();
        }
        return out;
    }

};

#endif //PPD_LAB4_CONCURRENTDOUBLYLINKEDLIST_H
